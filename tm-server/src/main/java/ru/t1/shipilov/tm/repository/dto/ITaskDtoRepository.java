package ru.t1.shipilov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}

